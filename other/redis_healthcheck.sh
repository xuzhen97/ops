#!/bin/bash
host="$(hostname -i || echo '127.0.0.1')"
if ping="$(redis-cli -h "$host" ping)" && [ "$ping" = 'PONG' ]; then
  echo "check success!"
  exit 0
elif ping="$(redis-cli -h "$host" -a "$1" ping)"  && [ "$ping" = 'PONG' ]; then
  echo "check success!"
  exit 0
fi
echo "check fail!"
exit 1
