# ops

## bash shell模块导入用法

下载我们的导入逻辑到用户目录下,函数

- import 下载脚本并source,缓存当天
- cleanImportCache 清除导入缓存

脚本统一路径避免麻烦

```bash
sudo curl -o /opt/.importrc https://gitee.com/xuzhen97/ops/raw/master/importrc
```

在相应的脚本中加载

```bash
source /opt/.importrc
# 远程脚本，可以更换其它的会执行source
import https://gitee.com/xuzhen97/ops/raw/master/common/common_fun.sh
```

## ssl_check.py

> ssl证书检查工具，主要用于检查阿里云配置的域名https证书有没有过期。

使用：

```bash
./ssl_check.py -d mydomain.com -p /mnt/d/mydomain.com.csv
```

mydomain.com.csv为阿里云导出的解析列表请自己保存为csv文件检查。

## common

[common](common/)

公共脚本。

## docker 文件夹

[docker](docker/)

docker相关的脚本。

## kubernetes 文件夹

[kubernetes](kubernetes/)

kubernetes相关的脚本。

## linux 文件夹

[linux](linux/)

linux相关的脚本。

## redis 文件夹

[redis](redis/)

redis相关的脚本
