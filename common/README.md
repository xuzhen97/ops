# common

## importrc

加载远程脚本，模块化开发

下载我们的导入逻辑到用户目录下,函数

- import 下载脚本并source,缓存当天
- cleanImportCache 清除导入缓存

脚本统一路径避免麻烦

```bash
sudo curl -o /opt/.importrc https://gitee.com/xuzhen97/ops/raw/master/importrc
```

在相应的脚本中加载

```bash
source /opt/.importrc
# 远程脚本，可以更换其它的会执行source
import https://gitee.com/xuzhen97/ops/raw/master/common/common_fun.sh
```

## common_fun.sh

公共脚本

```text
execFunc  执行函数例如execFunc main aaaa, 此时会执行脚本中的main函数，传入后面的参数
setExecFunc 设置execFunc函数，可以setExecFunc $*， 只有在其它文件应用，才能用这个函数
setDingtalk  可以使用执行函数执行，setDingtalk token 将token写入文件，脚本发送钉钉通知会用
sendMsgDingtalk 发送钉钉通知，sendMsgDingtalk msg
now 输出当前时间date "+%Y-%m-%d %H:%M:%S"
currFileName 获取当前文件名函数
logDebug 输出debug日志
openDebug 打开debug日志
closeDebug 关闭debug日志
logError 输出错误日志
logInfo 输出info级别日志
lock 锁，如果有人执行脚本在脚本，执行此函数加锁，如果不释放，则会等待，默认等待150s
unlock 释放锁文件
```

demo

```bash
#!/bin/bash
# Date: 2020.8.11
# Author: xuzhen
# Func: Docker container health recovery mechanism .
# V2.

source /opt/.importrc
import https://gitee.com/xuzhen97/ops/raw/master/common/common_fun.sh

# 健康检测方法, 约定0为正常
healthCheck(){
  status=$(docker inspect --format='{{.State.Health.Status}}' $1 2>&1)
  if [ "$status" == "unhealthy" ]; then
    return 1
  fi 
  return 0
}

# 健康检测失败动作
healthAction(){
  docker restart $1
}

# 主函数
main(){
  healthCheck $1
  # 当$?非0时代表健康检测失败，$?可以获取上个函数return的信息
  if [ "$?" -ne "0" ]; then
    sendMsgDingtalk $2
    healthAction $1
  fi
}

setExecFunc $*
main $1 $2
```

这个脚本在设置钉钉通知token后，就可以发送钉钉通知了，设置token也是调用导入脚本的函数，而关键点还是在于`setExecFunc $*`, 因为excFunc函数执行，下面的代码将不执行。
