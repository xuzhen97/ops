#!/bin/bash

source ./common_fun.sh

println(){
  echo $1
  echo $2
}

main(){
  local count=$(counting $$)

  if [ $count -lt 5 ]; then
    echo "循环次数不到！"
    sleep 1
    main
  fi
  uncounting $$
}

setExecFunc $*
main

# ./common_fun_test.sh execFunc sendMsgDingtalk "通知：测试！"
# ./common_fun_test.sh execFunc println  aa "bbcc"