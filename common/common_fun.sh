#!/bin/bash
# Date: 2020.8.21
# Author: xuzhen
# Func: 通用函数模块
# V1.


# bash脚本全路径，注意这个是根据执行传入的参数有关例如./执行cd .,如果是全路径/home/*.sh 则是
#    cd /home && pwd, 所有声明全局变量的形式，避免频繁切换cd 目录引起其它神奇bug.
SHELL_FULL_PATH="$( cd "$( dirname "$0"  )" && pwd  )"

# readFile 文件名 默认值, 读取的是脚本所在目录文件
readFile(){
  local fileDir=$SHELL_FULL_PATH
  if [ -f "$fileDir/$1" ] ; then
    for line in `cat $fileDir/$1`
    do
      echo $line
    done
  else
    echo $2
  fi
}

# 生成8位的id
generate8Id(){
  echo $RANDOM |md5sum |cut -c 1-8
}


# 执行函数例如execFunc main aaaa, 此时会执行脚本中的main函数，传入后面的参数
# 注意不支持传参带空格
execFunc(){
  local funcName=$1
  local params
  local i=0
  for p in $*               #在$*中遍历参数，此时每个参数都是独立的，会遍历$#次
  do
    ((i++))
    if [ "$i" -eq "2" ]; then
      funcName=$p
    fi
    if [ "$i" -gt "2" ]; then
      params="$params $p"
    fi
  done
  # 执行传入的函数
  "$funcName" $params
}

# 设置execFunc函数，可以setExecFunc $*
setExecFunc(){
  if [ "$1" = "execFunc" ]; then
    execFunc $*
    exit 0
  fi
}

# 设置钉钉机器人通知token, 重复设置会覆盖
setDingtalk(){
  local shellname=$(currFileName)
  local filename=$shellname"-access_token.info"
  echo $1 > $SHELL_FULL_PATH"/"$filename
}

# sendMsgDingtalk 要发送的消息，发送消息到钉钉消息，access_token请放在脚本同目录下的 脚本名-access_token.info 文件中
#   注意的是发送的消息中要带关键字
sendMsgDingtalk(){
 # $1 = msg
  if [ x"$1" = x ]; then
    logDebug "请传入发送信息，例如 sendMsg 这是一个悲伤的故事"
    return 1
  fi
  local cFile=$(currFileName)
  local accessToken=$(readFile $cFile"-access_token.info" "access_token")

  if [ "$accessToken" = "access_token" ]; then
    logDebug "钉钉通知没有配置！"
    return 1
  fi
  url="https://oapi.dingtalk.com/robot/send?access_token=$accessToken"
  curl -s $url \
    -H 'Content-Type: application/json' \
    -d '
      {
        "msgtype": "text",
        "text": {
          "content": "'"$1"'"
        }
      }' > /dev/null
}

# 当前时间
now(){
  echo $(date "+%Y-%m-%d %H:%M:%S")
}

# 当前文件名
currFileName(){
  local curr=$0
  echo ${curr##*/}
}

# debugInfo 调试输出信息，可以通过脚本下的 脚本名-debug.info 文件控制
#   存在文件且里面写入1则代表打印调试信息
logDebug(){
  local file=$(currFileName)
  local debugFile=$file"-debug.info"
  local debug=$(readFile $debugFile "0")
  if [ "$debug" -eq "1" ] ; then
    echo "$(now) debug "$1
  fi
}

# 打开脚本debug日志输出
openDebug(){
  local shellname=$(currFileName)
  local filename=$shellname"-debug.info"
  echo "1" > $SHELL_FULL_PATH"/"$filename
}

# 关闭脚本debug日志输出
closeDebug(){
  local shellname=$(currFileName)
  local filename=$shellname"-debug.info"
  rm -f $SHELL_FULL_PATH"/"$filename
}

# debugInfo 错误输出信息
logError(){
  echo "$(now) error "$1
}

# 普通info级别信息
logInfo(){
  echo "$(now) info "$1
}

# 获取锁文件
_getLockfile(){
  local path=$SHELL_FULL_PATH
  local name=$(currFileName)
  echo $path"/"$name"-lock.info"
}

_lockWait(){
  local lockfile=$(_getLockfile)
  local num=1
  while([ -f $lockfile ])
  do
    logDebug $lockfile"锁等待，第"$num"次"
    ((num++))
    # 等待10次，根据sleep,等待150秒，之后不在等待
    if [ $num -gt 30 ]; then
      break
    fi
    sleep 5
  done
  # 如果等待超时，锁没有释放则终止脚本
  if [ -f $lockfile ]; then
    logError "等待超时，终止脚本"$0
    exit 1
  fi
}

# 锁方法，如果有锁文件，执行脚本将会等待
lock(){
  _lockWait
  local lockfile=$(_getLockfile)
  echo "1" > $lockfile
}

# 解锁方法，标识已经执行完毕，其它进程可以在此执行脚本
unlock(){
  local lockfile=$(_getLockfile)
  rm -f $lockfile
}

# 进行计数
counting(){
  local shellname=$(currFileName)
  local filename=$shellname"-counting"$1".info"
  local num=$(readFile $filename "0")
  ((num++))
  echo "$num" > $SHELL_FULL_PATH"/"$filename
  echo $num
}

# 取消计数
uncounting(){
  local shellname=$(currFileName)
  local filename=$shellname"-counting"$1".info"
  rm -f $SHELL_FULL_PATH"/"$filename
}