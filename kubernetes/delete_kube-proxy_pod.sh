#!/bin/bash

delete() {
  for each in $(kubectl get pods -n $1| grep kube-proxy | awk '{print $1}');
  do
    kubectl delete pods -n $1 $each
  done
}

main() {
  delete kube-system
  echo "kube-proxy删除成功，会自动重新部署，生产环境勿用！"
}

main