#!/bin/bash

delete() {
  for each in $(kubectl get pods -n $1| grep Evicted | awk '{print $1}');
  do
    kubectl delete pods -n $1 $each
  done
}

main() {
  for each in $(kubectl get namespaces);
  do
    delete $each
    echo "清理空间$each成功！"
  done
}

main