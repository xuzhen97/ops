# kubernetes

## delete_evicted_pod.sh

根据kubectl命令删除所有空间下evicted状态的pod。

```bash
curl https://gitee.com/xuzhen97/ops/raw/master/kubernetes/delete_evicted_pod.sh|bash
```

## delete_kube-flannel-ds_pod.sh

根据kubectl删除所有kube-flannel-ds的pod, 这样就达到了重启的目的，正式环境需谨慎，最好看一下怎么无损重启，也就测试可以这么暴力。

```bash
curl https://gitee.com/xuzhen97/ops/raw/master/kubernetes/delete_kube-flannel-ds_pod.sh|bash
```

## delete_kube-proxy_pod.sh

根据kubectl删除所有的kube-proxy pod, 可以达到重启kube-proxy的目的，正式需谨慎，测试无所谓。

```bash
curl https://gitee.com/xuzhen97/ops/raw/master/kubernetes/delete_kube-proxy_pod.sh|bash
```
