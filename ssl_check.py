#!/bin/env python3
import subprocess
import os
import time
from datetime import datetime
import argparse
import csv
import sys
import json
import requests
import chardet


parser = argparse.ArgumentParser(description='本脚本检查https证书到期时间');
parser.add_argument('-d', '-domain', metavar='https网站，如www.jcici.com',required=True, dest='domain', help='输入监控的https网站')
parser.add_argument('-p', '-path', metavar='阿里云导出解析文件，请处理成csv,例如/home/aaa/bad.com.csv',required=True, dest='path', help='输入解析文件路径')
args = parser.parse_args()

def getFileEncoding(path):
  f = open(path, "rb")

  data = f.read()

  return chardet.detect(data).get("encoding") 

def checkSsl(www, days):
  # print(www, '证书到期天数是：', days)
  if int(days) < 30 :
    msg = www + "的https证书还有" + str(days) + "天过期了！"
    print(msg)

def getSslDays(www):
  comm1 = "curl https://"+www+" --connect-timeout 10 -v -s -o /dev/null 2>/tmp/ca.info ; cat /tmp/ca.info | grep 'start date: '" #利用curl检查证书开始时间，注意一下ca.info保存路径，connect-timeout可以控制超时时间，避免假死
  out_bytes1 = subprocess.check_output(comm1, shell=True)
  out_text1 = out_bytes1.decode('utf-8')
  comm2 = "cat /tmp/ca.info | grep 'expire date: '" #检查证书到期时间
  out_bytes2 = subprocess.check_output(comm2, shell=True)
  out_text2 = out_bytes2.decode('utf-8')
  notafter = datetime.strptime(out_text2[out_text2.find(":")+1:].strip(),"%b %d %H:%M:%S %Y GMT")
  remain_days = notafter - datetime.now()
  # print(www, '证书到期天数是：', remain_days.days)
  comm3 = "cat /tmp/ca.info | grep 'issuer: '" #获取证书颁发机构
  out_bytes3 = subprocess.check_output(comm3, shell=True)
  out_text3 = out_bytes3.decode('utf-8')
  # print(out_text3)
  os.system('rm -f /tmp/ca.info')
  time.sleep(1) #睡一会儿，免得太High
  return int(remain_days.days)

if not os.path.exists(os.path.abspath(args.path)):
  print("解析文件不存在！")
  sys.exit(1)


# checkSsl("www.dapengjiaoyu.com",100)

# sys.exit(1)
f = csv.reader(open(os.path.abspath(args.path),mode="r", encoding= getFileEncoding(os.path.abspath(args.path))))

for i in f:
  if i[6] == "正常":
    if i[0] == "A" or i[0] == "CNAME":
      www = args.domain
      if i[1] != "@":
        www = i[1] + "." + args.domain
      try:
        days = getSslDays(www)
        checkSsl(www,days)
      except Exception as e:
        pass