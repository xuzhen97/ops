#!/bin/bash
# Date: 2020.8.11
# Author: xuzhen
# Func: Docker container health recovery mechanism .
# V2.

source /opt/.importrc
import https://gitee.com/xuzhen97/ops/raw/master/common/common_fun.sh

# 健康检测方法, 约定0为正常
healthCheck(){
  status=$(docker inspect --format='{{.State.Health.Status}}' $1 2>&1)
  if [ "$status" == "unhealthy" ]; then
    return 1
  fi 
  return 0
}

# 健康检测失败动作
healthAction(){
  docker restart $1
}

# 主函数
main(){
  healthCheck $1
  # 当$?非0时代表健康检测失败，$?可以获取上个函数return的信息
  if [ "$?" -ne "0" ]; then
    sendMsgDingtalk $2
    healthAction $1
  fi
}

setExecFunc $*
main $1 $2

