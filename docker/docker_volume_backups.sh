#!/bin/bash
# author xuzhen97
# describe 备份容器的数据卷volumn，所有
usage() {
  echo "
  Usage:
    -p, --path 基础目录，不传入默认脚本执行目录，当然compose下有用
    -n, --name 容器name，如果不是compose需要传入
    -h, --help    命令帮助
    -c, --compose    compose模式，需要有docker-compose
  "
}

backups() {
  for each in $(docker inspect $1 | jq -c '.[0]|.Mounts|.[]|select(.Type=="volume")' | jq -r '.Name');
  do
    backupName="$each-`date +%Y%m`".tar
    docker run --rm \
      --volume $each:/tmp \
      --volume $(pwd):/backups \
      alpine \
      tar cvf /backups/$backupName /tmp
    echo $backupName
  done
}

composeBackups() {
  docker-compose stop
  for each in $(docker-compose ps | awk 'NR>2{print $1}');
  do
    echo "容器$each处理中。。。"
    backups $each
    echo "容器$each处理完成。。。"
  done
  docker-compose start
}

convert() {
  echo $1|sed $'s/\'//g'
}

main() {
  local name
  # 0 普通容器模式，1为compose模式
  local mode=0
  local path=$(pwd)
  while true; do
    case "$1" in
    -n | --name)
      name="$(convert $2)"
      shift
      ;;
    -p | --path)
      path="$(convert $2)"
      shift
      ;;
    -c | --compose)
      mode=1
      shift
      ;;
    -h | --help)
      usage
      exit
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "$1 is not option"
      break
      ;;
    esac
    shift
  done 
  if [ $mode == 0 ]
  then
    docker stop $name
    backups $name
    docker start $name
  else 
    cd $path
    composeBackups
  fi
}

set -- $(getopt -o n:,p:,c::,h --long name:,path:,compose::,help -- "$@")
main $@