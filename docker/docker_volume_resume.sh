#!/bin/bash
# author: xuzhen97
# desc: 恢复备份，需要手动创建好卷
VOLUME_NAME=$1
BACKUPS_NAME=$2

docker run --rm \
  --volume $VOLUME_NAME:/tmp \
  --volume $(pwd):/backups \
  alpine \
  tar xvf /backups/$BACKUPS_NAME -C /tmp --strip 1
