#!/bin/ash
# Date: 2020.8.11
# Author: xuzhen
# Func: Check whether the address returns 200, 
#       for example http://localhost:8081/actuator/health
# V1.

now=$(date "+%Y-%m-%d %H:%M:%S")
status=$(curl -s --connect-timeout 1 -m 3 -o /dev/null -w '%{http_code}' $1)

if [ $status -eq '200' ]; then
  echo "[$now] healthcheck success, http_code: "$status
  exit 0
else
  echo "[$now] healthcheck fail, http_code: "$status
  exit 1
fi
