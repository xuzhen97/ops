# docker

## docker_healthcheck.sh

docker健康检测，通过curl一个地址进行和docker指令的联动，来达到健康检测的效果。

## docker_log_clean.sh

docker容器日志清理

```bash
curl https://gitee.com/xuzhen97/ops/raw/master/docker/docker_log_clean.sh|bash
```

## docker_log_size.sh

docker 容器日志占比查看

```bash
curl https://gitee.com/xuzhen97/ops/raw/master/docker/docker_log_size.sh|bash
```

## docker_volume_backups.sh

docker数据卷备份脚本。

## docker_volume_resume.sh

docker 数据卷恢复脚本。

## health-recovery.sh

docker根据健康检测结果重启服务的脚本，但是说实话一旦出现效率问题，重启释放资源是一方面，扩容也是必须的，但是如果是因为代码问题，慢sql等，能回退则回退，多扩实例，抓紧优化了，因为量上来崩溃成了必然。
