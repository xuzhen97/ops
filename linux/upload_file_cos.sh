#!/bin/bash

# 工作目录，会将这个目录下所有tar上传，可以使用其它压缩包
WORKDIR=$1
# cos目录注意后面一定要有/,否则就是文件名
COSPATH=$2
cd $WORKDIR

# 过滤文件上传
ls *-`date +%Y%m`.tar | while read line
do
  echo $line
  docker run --name coscmd --rm -d \
    -v $(pwd):/data \
    -v /etc/resolv.conf:/etc/resolv.conf \
    -e SECRET_ID="SECRET_ID" \
    -e SECRET_KEY="SECRET_KEY" \
    -e BUCKET="BUCKET" \
    -e REGION="ap-beijing" \
    registry.cn-beijing.aliyuncs.com/xuzhen97/coscmd /data/$line  $COSPATH
done