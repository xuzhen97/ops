#!/bin/bash
# author: xuzhen97
# desc: 清理jenkins job build

# 检查是否至少传入了一个参数（不包括脚本名本身）
if [ "$#" -lt 1 ]; then
    echo "用法: $0 [jenkins job目录]"
    exit 1 # 非零退出状态码通常表示错误
fi

#jenkins 构建项目目录
# DIR="/home/user/.jenkins/jobs/"
DIR=$1

echo "clean dir $DIR"

#保留天数
OLDEST_DATE=$(date -d "$(date +%Y-%m-%d) -1 days" +%s)
 
# 添加项目 "Product" 和 "Dev"
# -name "Product" -o -name "Dev"
for DIRECTORY in $(find "$DIR" -maxdepth 1 -type d | sort); do
  if [ -d "$DIRECTORY/builds" ]; then
    echo "Processing $DIRECTORY/builds directory..."
    find "$DIRECTORY/builds" -maxdepth 1 -type d -name "[0-9]*" | while read LINE
    do
      MOD_DATE=$(stat -c %Y "$LINE")
      if [[ $MOD_DATE -lt $OLDEST_DATE ]]; then

        echo "Deleting old directory ${LINE}"
        ls -ld ${LINE}
        rm -rf "${LINE}"
      fi
    done
  else
    echo "Skipping $DIRECTORY/builds directory..."
  fi
done