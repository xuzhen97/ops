#!/bin/bash
# Date: 2020.8.14
# Author: xuzhen97
# Func: 日志备份脚本, 根据实际情况修改
# V1.

# 日志所在路径, 根据实际情况修改
LOG_PATH=/root/logs
# 日志备份路径，根据实际情况修改，区分年月，例如/data/log_bak/202008
LOG_BAK_PATH="/data/log_bak/`date +%Y%m`"

# 创建日志备份目录，如果有就不会在创建了
mkdir -p ${LOG_BAK_PATH}

# 进入日志目录将日志移动到备份目录，这里的日志格式例如 example.log.20200811
# 这里是假设项目的日志会每天一滚动的生成，格式如上示例
# 如果日志格式和项目不相同，请酌情修改
cd ${LOG_PATH}
mv *.log.* ${LOG_BAK_PATH}

# 进入日志备份目录，压缩日志，压缩采用指令gzip,解压使用gunzip,最终格式为.gz
cd ${LOG_BAK_PATH}

# ls的日志格式如同上面的格式，这里主要是为了跳过压缩文件，压缩过的不压缩
# 如有不同请酌情修改
ls *.log.* | while read line
do
	echo $line
  if [ "${line##*.}"x != "gz"x ];then
    gzip $line
  fi
done
echo `date "+%Y-%m-%d %H:%M:%S"`