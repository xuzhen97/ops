#!/bin/bash
# author: xuzhen97
# desc: 一个普通主机使用docker搭建引用，备份的例子

# 备份gitea, docker-compose部署
GITEA_PATH=/root/docker-apps/gitea
/opt/scripts/docker_volume_backups.sh -p $GITEA_PATH -c
/opt/scripts/upload_file_cos.sh $GITEA_PATH /gitea/

# 备份drone, docker-compose部署
DRONE_PATH=/root/docker-apps/drone
/opt/scripts/docker_volume_backups.sh -p $DRONE_PATH -c
/opt/scripts/upload_file_cos.sh $DRONE_PATH /drone/

# 备份nexus, docker-compose部署
NEXUS_PATH=/root/docker-apps/nexus
/opt/scripts/docker_volume_backups.sh -p $NEXUS_PATH -c
/opt/scripts/upload_file_cos.sh $NEXUS_PATH /nexus/

