# linux

## backups_file_to_cos.sh

主机使用docker搭建应用，并将卷备份copy到腾讯云cos上。

这个脚本主要是使用了docker文件夹下脚本`docker_volume_backups.sh`以及`upload_file_cos.sh`。

> 这里只是演示，这个脚本倒是没有太多实际意义。

## linux_audit_cmd.sh

德勤审计的时候要求执行的脚本，备份下。

## linux_clean_log.sh

linux日志的清除脚本，当日志占满磁盘总是要清理的吗，清理的时候考虑下是否需要备份，需要就提前备份下。

## log_bak.sh

一个日志文件备份压缩且根据日期命名的例子，不过需要注意当你将文件移走之后还会不会在写的问题，有些应用可能处理下。

## upload_file_cos.sh

一个可以将文件上传到腾讯云cos上的脚本，依赖自己打包的镜像，这样我个人认为有个好处，主机除了安装docker, 不需要安装各种环境，这样就避免了将服务器搞坏的机率。

## jenkins_clean_job.sh

清理1天以上的构建文件。

```bash
curl -sL https://gitee.com/xuzhen97/ops/raw/master/linux/jenkins_clean_job.sh | bash -s -- "/home/user/.jenkins/jobs/"
```